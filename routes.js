const http = require('http');

const port = 4000;

const server = http.createServer((req,res)=>{
    //Accessing the "greetin" rout returns a massage of  "Hello World"

    if(req.url == '/greeting'){
        res.writeHead(200, {'Content-Type':'text/plain'})
        res.end("Hello World!")
    }else if(req.url == '/homepage'){
        res.writeHead(200,{'Content-Type' : 'text/plain'})
        res.end("Welcome to our homepage")
    }else {
        //set a status code for the response - a 404 means "NOT FOUND"
        res.writeHead(404, {'Contetn-Type': 'text/plain'})
        res.end("Page not Available")
    }
})

server.listen(port)
console.log(`Server now accessible at localhost ${port}`);