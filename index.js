// Use the "require" directive to load Node.js module
//a "module" is a software component or part of a program that contains one or more routines
//"http module" lets node.js transfer data using the hyper text transfer protocol

let http = require("http");

http.createServer(function (request, response){
    //Use the writeHead() method to:
     //Set a status code for the response
     //Set the content-type of the response as a plain text message
    response.writeHead(200,{'Content-type':'text/plain'})
    response.end("B245 Hello!");

}).listen(4000);
//When server is running, console wil print the message
console.log("Server running at localhost: 4000")

